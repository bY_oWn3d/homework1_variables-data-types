//Теоретичні питання

//1.Як можна оголосити змінну у Javascript? (var, let можна переприсвоювати нове значення, const не можна переприсвоювати нове значення)

let greetingsForUsers = 'Hello my dear user';
greetingsForUsers = 'Hello world';  // Переприсвоєння 
alert(greetingsForUsers);
console.log(greetingsForUsers);

const greetingsForUsers1 = 'Hello user' // Не можна присвоїти нове значення
alert(greetingsForUsers1);
console.log(greetingsForUsers1);

//2.У чому різниця між функцією prompt та функцією confirm?
//Функція prompt приймає два аргументи: Вона показує модальне вікно з текстовим повідомленням, полем, куди відвідувач може ввести текст, та кнопками ОК/Скасувати.
//Функція confirm показує модальне вікно з питанням question та двома кнопками: ОК та Скасувати. Результат: true, якщо натиснути кнопку OK, Cancel — false.

const isAdmin = confirm('Are you Admin?');
alert(isAdmin)
console.log(isAdmin);

//prompt Наведенний у третьому завданні


//3.Що таке неявне перетворення типів? Наведіть один приклад.
// Типи данних 8 (Примітивні типи - String, Number, Boolean, Null, Undefined, Big Int, Symbol), окремий тип Object

String(123) // Явне перетворення
123 + ''    // Неявне перетворення

//Завдання

//1.Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

let name 
let admin 
name = 'Valentyn'
admin = name
console.log(admin);

//2.Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

let days = 3;
let daysInSeconds = days * (24 * 60 * 60);
console.log(daysInSeconds);

//3. Запитайте у користувача якесь значення і виведіть його в консоль.

let country = prompt("Яку країну бажаєте відвідати?", "Ukraine");
alert(country);
console.log(country);